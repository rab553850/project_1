 # VPC CIDR VARIABLE
variable "vpc_cidr" {
  type        = string
  description = "the vpc cidr variable"
  default     = "10.0.0.0/16"

}

# SUBNETS CIDR VARIABLES #
# Public_subnet1 cidr block
variable "Public_subnet1_cidr" {
  type        = string
  description = "Public_subnet1 cidr block"
  default     = "10.0.1.0/24"

}


# Public_subnet2 cidr block
variable "Public_subnet2_cidr" {
  type        = string
  description = "Public_subnet2 cidr block"
  default     = "10.0.2.0/24"

}

# Private_subnet1 cidr block
variable "Private_subnet1_cidr" {
  type        = string
  description = "Private_subnet1_cidr block"
  default     = "10.0.3.0/24"

}

# Private_subnet2 cidr block
variable "Private_subnet2_cidr" {
  type        = string
  description = "Private_subnet1_cidr block"
  default     = "10.0.4.0/24"

}

# Setting AZ_2a as variable
variable "AZ_2a" {
  type        = string
  description = "name of availability zone 2a"
  default     = "us-west-2a"

}

# Setting AZ_2b as variable
variable "AZ_2b" {
  type        = string
  description = "name of availability zone 2b"
  default     = "us-west-2b"

}

# Setting Region as variable
variable "region_name" {
  type        = string
  description = "name of region"
  default     = "us-west-2"
}

# Setting "aws_route" "public_route_table_assoc" cidr as variable
variable "public_route_table_assoc_cidr" {
  type        = string
  description = "pubiic_route_table_assoc_cidr"
  default     = "0.0.0.0/0"

}